# Verkkokauppajärjestelmä - Example project

This is an example project for the SAT part of the Web degree at [Taitotalo](https://www.taitotalo.fi).

This project uses:
- [Express JS](https://expressjs.com/) for web server handling.
- [EJS](https://ejs.co/) as the template system.
- [Sequelize](https://sequelize.org/) with [SQLite](https://github.com/mapbox/node-sqlite3) backend as database management system.
- [bcrypt](https://github.com/kelektiv/node.bcrypt.js) to encrypt user passwords.
- [Express-session](https://github.com/expressjs/session) to manage user sessions.
- [Connect-session-sequilize](https://github.com/mweibel/connect-session-sequelize) to store session information in a SQLite database.
- [Express-validator](https://express-validator.github.io/docs/) to validate and sanitize form input.
- [Multer](https://github.com/expressjs/multer) to allow image uploads to the site.

## To use this project

Of course first of all [Node.js](https://nodejs.org/) has to be installed in your system.

Then, you have two options:
1. You can clone the project with git. In a command line terminal (`cmd`, `PowerShell`, `bash`, ...), move (`cd`) to the folder where you want to have this project and then execute:
```
git clone git@gitlab.com:cfinnberg/verkkokauppa_node.git VerkkoKauppa_Node
cd VerkkoKauppa_Node
```

2. You can also download the [zip](https://gitlab.com/cfinnberg/verkkokauppa_node/-/archive/main/verkkokauppa_node-main.zip) version and uncompress it to a folder. Then open a command line terminal (`cmd`, `PowerShell`, `bash`, ...) at that folder (or move to that folder with `cd`).

In any case, when you have the files in a local folder, execute the following to initialize and start your project:
```
npm install
npm install -g nodemon
nodemon
```

And finally you can open the project in the browser with this address: [http://localhost:3000/](http://localhost:3000/)

## Prototype

It is very recommended to have a prototype of your website before you start the implementation of your website with Node JS.
The prototype of this project is at: [https://gitlab.com/cfinnberg/djangokauppa_proto](https://gitlab.com/cfinnberg/djangokauppa_proto). There is also an [online version](https://cfinnberg.gitlab.io/djangokauppa_proto/etusivu.html) of the proto available.

## Disclaimer

This project is incomplete and is still under development but it will never be a viable online store solution. It is just an example on how to built an online service with Node.js and some libraries.