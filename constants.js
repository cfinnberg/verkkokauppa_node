const path = require('path');


let publicFolderName = 'public';
let uploadsFolderName = 'uploads';
let uploadsFolderURL = path.join('/', uploadsFolderName);
let uploadsFolderRelPath = path.join(publicFolderName, uploadsFolderName);

module.exports = { publicFolderName, uploadsFolderName, uploadsFolderURL, uploadsFolderRelPath }