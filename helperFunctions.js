module.exports = {
    // Create a new object with better organization for the form's error messages
    errorMessages: errors => {
        messages = {}
        errors.errors.forEach(error => {
            if (messages[error.param]) {
                messages[error.param].push(error.msg);               
            } else {
                messages[error.param] = [error.msg];
            }
        });
        return messages;
    }
}