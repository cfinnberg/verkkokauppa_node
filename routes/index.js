var express = require('express');
var router = express.Router();
const { Tuote } = require('../models');

/* GET Etusivu */
router.get('/', async (req, res, next) => {
  var tuote;

  // Get user if user is logged in
  let user = req.session.user ? req.session.user : null;

  // The homepage shows a highlighted product (if there are any product in the database)
  // Get a list of all products
  tuotteet = await Tuote.findAll();
  if (tuotteet.length > 0) {
    // If the list is not empty, get the index of a random product in the list
    let tuoteIndex = Math.floor(Math.random() * tuotteet.length);
    // Get the product
    tuote = tuotteet[tuoteIndex];
  } else {
    // If the list is empty, there is no product to show
    tuote = null;
  }
  res.render('etusivu', { otsikko: 'Etusivu', tuote: tuote, user: user });
});

/* GET Yhteytiedot */
router.get('/yhteystiedot', (req, res, next) => {
  // Get user if user is logged in
  let user = req.session.user ? req.session.user : null;

  res.render('yhteystiedot', { otsikko: 'Yhteystiedot', user: user });
});

/* GET Tietosuojaseloste */
router.get('/tietosuojaseloste', (req, res, next) => {
  // Get user if user is logged in
  let user = req.session.user ? req.session.user : null;
  
  res.render('tietosuojaseloste', { otsikko: 'Tietosuojaseloste', user: user });
});

/* Some GET shortcuts */
router.get('/login', (req, res, next) => {
  res.redirect('/kayttajat/kirjautuminen');
});
router.get('/logout', (req, res, next) => {
  res.redirect('/kayttajat/uloskirjautuminen');
});

module.exports = router;
