var express = require('express');
var router = express.Router();

// Asiakas database model
const { Asiakas } = require('../models');

// Password encrypation
const bcrypt = require('bcrypt');

// Form input validation
const { body, validationResult } = require('express-validator');

const { errorMessages } = require('../helperFunctions');

// Helper function to log in user (create user session)
// It is in its own function because same code it's used in two different places
function login(session, käyttäjä) {
  session.user = {
    id: käyttäjä.id,
    etunimi: käyttäjä.etunimi,
    isAdmin: käyttäjä.isAdmin
  }
}


// ****
// ****
// **** Rekisteröinti
// ****
// ****

// Helper function to validate registration form input
validateRegistrationInput = [
  // Username is not empty, min length of 4, it's trimmed and lowercasered
  body('username')
    .not().isEmpty().withMessage('Käyttäjätunnus on pakollinen')
    .trim()
    .isLength({min: 4}).withMessage('Käyttäjätunnuksen minimi pituus on 4 merkkiä')
    .isLength({max: 150}).withMessage('Käyttäjätunnuksen maksimi pituus on 150 merkkiä')
    .toLowerCase(),
  // Username must use only latin letter and numbers.
  body('username').custom( value => {
    if (value.match(/^[0-9a-z]+$/)) {
      return true
    } else {
      return Promise.reject('Kielettyä merkkiä käyttäjätunnuksessa. Vain a-z ja 0-9 salittu.')
    }
  }),
  // Username is not already taken
  body('username').custom( async käyttäjätunnus => {
    if (await Asiakas.findOne({ where: {käyttäjätunnus: käyttäjätunnus}})) {
      return Promise.reject('käyttäjätunnus on jo olemassa');
    } else {
      return true;
    }
  }),
  // First name is not empty and it's escaped
  body('first_name')
      .trim()
      .escape(),
  // Same to last name
  body('last_name')
      .trim()
      .escape(),
  // email must be an email and it's trimmed
  body('email')
      .trim()
      .isEmail(),
  // Email is not already taken
  body('email').custom( async email => {
      if (await Asiakas.findOne({ where: {email: email}})) {
        return Promise.reject('Sähköpostiosoite on jo olemassa');
      } else {
        return true;
      }
    }),
  // password must be at least 5 chars long
  body('password1')
      .isLength({ min: 8 }).withMessage('Salasanan minimi pituus on 6 merkkiä.'),
  body('password1').custom((value, { req }) => {
      if (value !== req.body.password2) {
          throw new Error('Salasanat eivät täsmä');
      } else {
          return true
      }
  })
];

/* GET Rekisteröinti page */
router.get('/rekisterointi', (req, res, next) => {
  // If user is logged in, redirect to the homepage
  if (req.session.user) { res.redirect('/'); }

  res.render('rekisteröinti', { otsikko: 'Rekisteröinti', form: {}, errors: {}, user: null });
});

/* POST Rekisteröinti page */
router.post('/rekisterointi', validateRegistrationInput, async (req, res, next) => {
  // if user is already logged in. Redirect to the homepage
  if (req.session.user) { res.redirect('/') }

  // Process validation errors
  const errors = errorMessages(validationResult(req));
  console.log(JSON.stringify(errors));

  // If there are errors then show again the form (maintaining the data) and corresponding error messages
  if (Object.keys(errors).length != 0) {
    res.render('rekisteröinti', { otsikko: 'Rekisteröinti', form: req.body, errors: errors, user: null });
    return
  }

  // At this point, the form is considered valid and the user should be created

  // Prepare password encryption
  const saltRounds = 10;
  const salt = bcrypt.genSaltSync(saltRounds);
  const hash = bcrypt.hashSync(req.body.password1, salt);
  
  // Create user and sync the database
  käyttäjä = await Asiakas.create({
      käyttäjätunnus: req.body.username,
      etunimi: req.body.first_name,
      sukunimi: req.body.last_name,
      email: req.body.email,
      salasana: hash,
      isAdmin: (await Asiakas.count() == 0)   // If this is the first user, then is an admin
  });
  
  // Login the user at the same time
  login(req.session, käyttäjä);
  
  // And redirect the user to the homepage
  res.redirect('/');
});


// ****
// ****
// **** Kirjaudu sisään
// ****
// ****

/* GET Login page */
router.get('/kirjautuminen', (req, res, next) => {
  // if user is already logged in. Redirect to the homepage
  if (req.session.user) { res.redirect('/') }

  res.render('kirjaudu', { otsikko: 'Kirjaudu', form: {}, errors: false, user: null });
});

/* POST Login page */
router.post('/kirjautuminen', async (req, res, next) => {
  // if user is already logged in. Redirect to the homepage
  if (req.session.user) { res.redirect('/') }

  // Find the user in the database
  käyttäjä = await Asiakas.findOne({ where: {käyttäjätunnus: req.body.username}});
  if (käyttäjä) {
    // if the user exists, compare the password
    if ( await bcrypt.compare(req.body.password, käyttäjä.salasana) ) {
      // if password is correct, create the user session
      login(req.session , käyttäjä)
      // And redirect the user to the homepage
      res.redirect('/');
    }
  };
  
  // if username or password is incorrect reload login page and show error
  res.render('kirjaudu', { otsikko: 'Kirjaudu', form: {}, errors: true, user: null });
});


// ****
// ****
// **** Kirjaudu ulos
// ****
// ****

/* GET Uloskirjautuminen page */
router.get('/uloskirjautuminen', (req, res, next) => {
  // If user is logged in (if user property exists in the session), delete session
  if (req.session.user) {
    req.session.destroy();
  }

  // In any case redirect the user to the homepage
  res.redirect('/');
});


module.exports = router;
