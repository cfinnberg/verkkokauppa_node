const express = require('express');
const router = express.Router();
const path = require('path');
const { Tuote } = require('../models');
const { Op } = require("sequelize");
const fs = require('fs');
const { uploadsFolderRelPath } = require('../constants');
const { errorMessages } = require('../helperFunctions');

// Form input validation
const { body, validationResult } = require('express-validator');

// For image upload
var multer  = require('multer')
var upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => cb(null, uploadsFolderRelPath),
        filename: (req, file, cb) => {
          const index = file.originalname.lastIndexOf('.');
          const filename = file.originalname.substring(0,index);
          const ext = file.originalname.substring(index);
          return cb(null, `${filename}-${Date.now()}-${Math.round(Math.random() * 1E9)}${ext}`);
        }
    }),
    fileFilter: (req, file, cb) => {
        let acceptedMimeTypes = ["image/png", "image/jpg", "image/jpeg", "image/webp"];
        cb(null, acceptedMimeTypes.includes(file.mimetype))
    }
});


// ****
// ****
// **** Tuotteiden lista
// ****
// ****

/* GET /Tuotteet */
router.get('/', (req, res, next) => {
    res.redirect(req.baseUrl + '/kaikki');
});

/* GET kaikki tuotteet */
router.get('/kaikki', async (req, res, next) => {
    // Get user if user is logged in
    let user = req.session.user ? req.session.user : null;
    
    // Get a list of all products
    let tuotteet = await Tuote.findAll();
    
    res.render('tuotteet', { otsikko: 'Tuotteet', tuotteet: tuotteet, user: user });
});

// ****
// ****
// **** Lisätä tuotetta
// ****
// ****

/* GET lisätä tuote sivu */
router.get('/uusi', (req, res, next) => {
    // Get user if user is logged in
    let user = req.session.user ? req.session.user : null;
    
    // If user is not logged in or user is not admin, redirect user to the homepage
    if (!user || !user.isAdmin) { res.redirect('/'); }
    
    // Render form without extra messages
    res.render('lisätätuote', { otsikko: 'Lisätä tuote', user: user, errors: {}, form: {}});
});

// Helper function to validate product form input
validateProducInput = [
    // Product name is not empty, min length of 4, it's trimmed and escaped
    body('nimi')
        .not().isEmpty().withMessage('Tuotteen nimi on pakollinen')
        .trim().escape()
        .isLength({min: 4}).withMessage('Tuotteen nimi minimi pituus on 4 merkkiä')
        .isLength({max: 100}).withMessage('Käyttäjätunnuksen maksimi pituus on 100 merkkiä'),
    // Product name is not already taken
    body('nimi').custom( async nimi => {
        if (await Tuote.findOne({ where: {nimi: {[Op.like]: nimi}}})) {
            return Promise.reject('Tuotteen nimi on jo olemassa');
        } else {
            return true;
        }
    }),
    // Product description is not empty and it's escaped
    body('lisätiedot')
        .not().isEmpty().withMessage('Tuotteen lisätiedot ovat pakollista')
        .trim()
        .escape(),
    // Price must be a positive number (decimals allowed)
    body('hinta').custom( hinta => {
        hinta = hinta.replace(',','.');
        if (+hinta && +hinta > 0) {
            return true;
        } else {
            return Promise.reject('Hinta pitää olla positiivinen luku');
        }
    }),
    // Quantity must be a positive integer (no decimals allowed)
    body('määrä').custom( määrä => {
        if (määrä.match(/^[0-9]+$/)) {
            return true;
        } else {
            return Promise.reject('Määrä pitää olla positiivinen kokonaisluku');
        }
    })
];

/* POST lisätä tuote sivu */
router.post('/uusi', upload.single('kuva'), validateProducInput, async (req, res, next) => {
    // Get user if user is logged in
    let user = req.session.user ? req.session.user : null;
    
    // if there is an image attached, req.file has file information. If not, req.file is undefined
    
    // If user is not logged in or user is not admin, redirect user to the homepage
    if (!user || !user.isAdmin) { res.redirect('/'); }
  
    // Process validation errors
    const errors = errorMessages(validationResult(req));
    const success = (Object.keys(errors).length == 0);
    
    if (success) {
        // Create product and sync database
        await Tuote.create({
        nimi: req.body.nimi,
        lisätiedot: req.body.lisätiedot,
        hinta: req.body.hinta,
        kuva: req.file.filename,
        määrä: req.body.määrä
        });
    } else {
        // Remove file if uploaded
        if (req.file) {
            fs.unlink(path.join(uploadsFolderRelPath, req.file.filename), ()=>{}); // Do nothing if error.
        }
    }
        
    // If everything went right, render form with successful message. If not show again the form (maintaining the data) and corresponding error messages
    res.render('lisätätuote', { otsikko: 'Lisätä tuote', user: user, errors: errors, success: success, form: success ? {}:req.body });
});


// ****
// ****
// **** Tuotteen yksityiskohdat
// ****
// ****

/* GET Yhkistyiskohat tuotteesta sivu. */
router.get('/yksityiskohdat/:id', async (req, res, next) => {
    // Get user if user is logged in
    let user = req.session.user ? req.session.user : null;

    // Get product by its id (Primary Key)
    let tuote = await Tuote.findByPk(req.params.id);
    
    // Render product details page
    res.render('tuote_yksityiskohdat', { otsikko: 'Tuotteen yksityiskohdat', tuote: tuote, user: user });
});

module.exports = router;
