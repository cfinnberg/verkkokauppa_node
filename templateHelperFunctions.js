const path = require('path');
const { uploadsFolderURL } = require('./constants.js');


module.exports = (locals) => {
    locals.truncate = (teksti, pituus) => {
    return (teksti.length > pituus-3) ? teksti.slice(0,pituus)+'...' : teksti;
    }

    locals.parseDate = (date) => {
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();
        min = date.getMinutes();
        hour = date.getHours();
        
        return `${day}.${month}.${year} ${hour}:${min}`
    }

    locals.price = (price) => {
        return String(price.toFixed(2)).replace('.',',')
    }
    
    locals.uploaded = (file) => {
        return path.join(uploadsFolderURL, file)
    }
    
    return locals
}
