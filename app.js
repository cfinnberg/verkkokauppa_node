var createError = require('http-errors');
var express = require('express');
var path = require('path');
// cookieParser is not required by express-session anymore
// var cookieParser = require('cookie-parser'); 

// Logging module
var logger = require('morgan');


////////////////////////////////
/////  Session management  /////
////////////////////////////////
// express-session is required to handle user sessions
var session = require('express-session');

// Option 1: Sessions are stored in memory:
// This is actually not needed, as it is the default. Just omit store param in session module initialization.
// var MemoryStore = session.MemoryStore;
// var myStore = new MemoryStore({
//   checkPeriod: 86400000 // prune expired entries every 24h
// });

// Option 2: Session management with sqlite database:
var Sequelize = require("sequelize");
var sequelizeSession = new Sequelize({
  dialect: "sqlite",
  storage: "./session.sqlite",
});
var SequelizeStore = require("connect-session-sequelize")(session.Store);
var myStore = new SequelizeStore({
  db: sequelizeSession,
});
myStore.sync();


// Import some routes into variables to use later
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/käyttäjät');

// Initialize the express app.
var app = express();

// Set 'views' as the default folder for templates
app.set('views', path.join(__dirname, 'views'));
// Set '.ejs' as the default extension for templates
app.set('view engine', 'ejs');

// logging
app.use(logger('dev'));

// Add capability to understand the body part of the request in a json post request.
app.use(express.json());

// Add capability to understand the body part of the request in a form post request. Body is available as req.body
app.use(express.urlencoded({ extended: false }));

// cookieParser is not required by express-session anymore
// app.use(cookieParser());

// Initialize session module using selected store management above and set express to use it.
app.use(session({
  secret: 'MySecret',
  store: myStore,
  resave: false,
  saveUninitialized: false
}));

// Set 'public' as static folder
const { publicFolderName } = require('./constants.js');
app.use(express.static(publicFolderName));

// Add paths in '/'
app.use('/', indexRouter);
// Add paths in '/users'
app.use('/kayttajat', usersRouter);
// Add path in '/tuotteet' without any middle variable.
app.use('/tuotteet', require('./routes/tuotteet'));

// Add helper functions to app.locals (necessary for some templates)
app.locals = require('./templateHelperFunctions')(app.locals);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
